using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Created By Atiyeh Norouzzadeh
/// This script is responsible for managing the time that player has to be reached to one of the deactive lamps,
/// if the player failed to reach it will lose his current position in map and appear in a random point.
/// </summary>
namespace Assignments.StruckInLoop
{
    public class GameController : MonoBehaviour
    {
        #region SERILIZEDFIELD FIELDS
        [SerializeField]
        private List<Lamp> deactiveLamps;
        [SerializeField]
        private GameObject character;
        [SerializeField]
        private AudioClip reachTimeAudio;
        [SerializeField]
        private Scrollbar timeSlider;
        #endregion

        #region PRIVATE FIELDS
        private AudioSource audioSource;
        private float timeSession = 12;
        private float decreaseTime = 0.5f;
        private float timeCounter;
        private Lamp currentLamp;
        private bool timesUp;
        private bool reachedLamp;
        private static GameController _instance;
        #endregion

        #region PROPERTIES
        public static GameController Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<GameController>();
                return _instance;
            }
        }
        #endregion

        #region MONO BEHAVIOURS
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.Play();
        }

        void Update()
        {
            if (!timesUp)
            {
                timeCounter += Time.deltaTime / timeSession;
                timeSlider.size = timeCounter;
                if (timeCounter >= 1)
                {
                    ResetValues();
                    StartCoroutine(DisappearCoroutine());
                }
                if (reachedLamp)
                {
                    timeSession -= decreaseTime;
                    ResetValues();
                    StartCoroutine(ReachedRoutine());
                }
            }
        }
        #endregion

        #region PUBLIC METHODS
        public void PointReached(Lamp lamp)
        {
            deactiveLamps.Remove(lamp);
            reachedLamp = true;
        }

        public void SetCurrentLamp(Lamp lamp)
        {
            currentLamp = lamp;
        }

        public void EndGame()
        {
            ResetValues();
        }
        #endregion

        #region PRIVATE METHODS
        void ResetValues()
        {
            timesUp = true;
            audioSource.Stop();
            timeCounter = 0;
            timeSlider.size = 0;
        }

        void SetValues()
        {
            audioSource.Play();
            timesUp = false;
            reachedLamp = false;
        }

        void SetNewPoint()
        {
            var lampIndex = deactiveLamps.IndexOf(currentLamp);
            var random = Random.Range(0, lampIndex);
            character.transform.position = deactiveLamps[random].GetNearestPoint().transform.position;
            timesUp = false;
        }

        IEnumerator ReachedRoutine()
        {
            yield return new WaitForSeconds(2f);
            SetValues();
        }

        IEnumerator DisappearCoroutine()
        {
            yield return new WaitForSeconds(3f);
            SetNewPoint();
            SetValues();
        }
        #endregion
    }
}