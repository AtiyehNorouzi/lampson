using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Assignments.StruckInLoop
{
    public class UIText : MonoBehaviour
    {
        #region SERILIZED FIELDS
        [SerializeField]
        private Text text;
        #endregion

        #region PRIVATE METHODS
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "character")
            {
                text.gameObject.SetActive(true);
                GameController.Instance.EndGame();
            }
        }
        #endregion
    }
}
