using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assignments.StruckInLoop
{
    public class Lamp : MonoBehaviour
    {
        #region SERILIZEDFIELD FIELDS
        [SerializeField]
        private GameObject pointLight;
        [SerializeField]
        private GameObject spotLight;
        [SerializeField]
        private GameObject nearestPoint;
        #endregion

        #region PRIVATE FIELDS
        State LampState = State.Off;
        #endregion

        #region ENUMS
        enum State
        {
            On,
            Off
        }
        #endregion

        #region PUBLIC METHODS
        public GameObject GetNearestPoint()
        {
            return nearestPoint;
        }

        public void SwitchState()
        {
            if (LampState == State.Off)
            {
                LampState = 1 - LampState;
                pointLight.SetActive(true);
                spotLight.SetActive(true);
                GameController.Instance.PointReached(this);
            }
        }
        #endregion
    }
}
