using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Created By Atiyeh Norouzzadeh
/// This script is for controlling the characters collisions and triggers with other objects.
/// </summary>
namespace Assignments.StruckInLoop
{
    public class CharacterTrigger : MonoBehaviour
    {
        #region PRIVATE METHODS
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "lamp")
            {
                var hitLamp = other.gameObject.GetComponent<Lamp>();
                hitLamp.SwitchState();
                GameController.Instance.SetCurrentLamp(hitLamp);
            }

        }
        #endregion
    }
}
